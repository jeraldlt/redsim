from redsim import *
from math import pi
rx_range = [0.0, 10.0]
ry_range = [0.0, 10.0]
vx_range = [0.0, 20.0]
vy_range = [0.0, 20.0]


u = User([5.0,5.0], pi/2.0, [10.0,10.0], pi/2.0)
r = ConstantRedirector([u], 0.75, 0.5, 1.0/7.5)
res = ToCenterResetter([u], 1.0, 1.0, rx_range, ry_range, 1.0)
g = GUI(rx_range, ry_range, vx_range, vy_range)
s = Simulation([u], r, res, g)

s.run()
