from redsim import *

rx_range = [0.0, 10.0]
ry_range = [0.0, 10.0]
vx_range = [0.0, 20.0]
vy_range = [0.0, 20.0]


u = User([5.0,5.0],[1.0,0.0],[10.0,10.0],[1.0,0.0])
r = S2CRedirector(1, 0.5, 1.5, u.configuration(), u.configuration_rot(), [5.0,5.0])
g = GUI(rx_range, ry_range, vx_range, vy_range)
s = Simulation([u], r, g)

s.run()
