"""
Utility functions
"""

#
#
#

from math import sin, cos

def distance(x1, x2):
    return sum([(x2[i] - x1[i]) ** 2 for i in range(len(x1))]) ** 0.5
    
def normalize(x):
    norm = float(sum([a**2 for a in x]) ** 0.5)
    if norm == 0.0:
        return [0.0 for a in x]
    return [a/norm for a in x]
    
def rot_vector(rot):
    return [cos(rot), sin(rot)]
    
def clamp(value, lower_bound, upper_bound):
    if value <= lower_bound:
        return lower_bound
    if value >= upper_bound:
        return upper_bound
    return value
    
