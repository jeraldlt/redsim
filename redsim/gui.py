"""
GUI component for simulation
"""

# Copyright (C) Jerald Thomas - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jerald Thomas <jeraldlt@usc.edu>, Fall 2016

import pygame
import sys

"""
pygame.init()
s=pygame.Surface((640,480))
s.fill((64,128,192,224))
pygame.draw.circle(s,(255,255,255,255),(100,100),50)

app=QtGui.QApplication(sys.argv)
w=MainWindow(s)
w.show()
app.exec_()
"""

class GUI():
    def __init__(self, rx_range, ry_range, vx_range, vy_range, title="RedSim"):

        self.rx_range = rx_range
        self.ry_range = ry_range
        self.vx_range = vx_range
        self.vy_range = vy_range
        
        self.WIDTH = 1200
        self.HEIGHT = self.WIDTH/2
        self.FRAMERATE = 60
        self.MB_LEFT = 1
        self.MB_RIGHT = 3

        
        self.ppm_v = self.HEIGHT / vx_range[1]
        self.ppm_r = self.HEIGHT / vx_range[1]


        self.screen = pygame.display.set_mode((self.WIDTH, self.HEIGHT))
        self.canvas = pygame.Surface((self.WIDTH, self.HEIGHT))
        self.view_virtual = self.canvas.subsurface((0, 0, self.WIDTH/2, self.HEIGHT))
        self.view_real = self.canvas.subsurface((self.WIDTH/2, 0, self.WIDTH/2, self.HEIGHT))
        
        pygame.display.set_caption(title)





    def draw_background(self):

        
        # Draw environment background
        self.view_virtual.fill((150, 150, 250))
        self.view_real.fill((150, 150, 150))
        pygame.draw.rect(self.view_real, (150,250,150), ((self.rx_range[0]*self.ppm_r), (self.ry_range[0]*self.ppm_r), self.rx_range[1]*self.ppm_r, self.ry_range[1]*self.ppm_r))
        
        # Draw reference axis
        pygame.draw.rect(self.view_virtual, (0,0,0), (5, self.HEIGHT-10, self.ppm_v, 5))
        pygame.draw.rect(self.view_real, (0,0,0), (5, self.HEIGHT-10, self.ppm_r, 5))
        pygame.draw.line(self.view_virtual, (0,200,0), (5, 5), (5, 30))
        pygame.draw.line(self.view_virtual, (200,0,0), (5, 5), (30, 5))
        pygame.draw.line(self.view_real, (0,200,0), (5, 5), (5, 30))
        pygame.draw.line(self.view_real, (200,0,0), (5, 5), (30, 5))
    

        

    def draw_finished(self):   
            
        self.screen.blit(self.canvas, (0,0))
        pygame.display.update()
            
            

    
    def in_range(x, x_range):
        if x < x_range[0]:
            return False
        if x > x_range[1]:
            return False
        return True

    def in_virtual(self, pos):
        if pos[0] < 0:
            return False
        elif pos[0] > self.WIDTH/2:
            return False
        elif pos[1] < 0:
            return False
        elif pos[1] > self.HEIGHT:
            return False
        return True
        
    def in_real(self, pos):
        if pos[0] < self.WIDTH/2:
            return False
        elif pos[0] > self.WIDTH/2 + int(self.rx_range[1]*self.ppm_r):
            return False
        elif pos[1] < 0:
            return False
        elif pos[1] > int(self.ry_range[1]*self.ppm_r):
            return False
        return True  

