"""

"""

#
#
#

import pygame
from math import cos, sin, atan2
from util import *

class Simulation(object):
    def __init__(self, users, redirector, resetter, gui):
        
        self.users = users
        #self.xs = []
        #self.update_configuration()
        
        self.redirector = redirector
        self.resetter = resetter
        self.gui = gui
        
        
        self.update_rate = 60   # Hz
        
        pygame.init()
        self.clock = pygame.time.Clock()
        self.running = True        


    def handle_events(self):
        event = pygame.event.poll()
        if event.type == pygame.QUIT:
            self.running = False
        #elif event.type == pygame.KEYDOWN:
        
        for user in self.users:
            user.handle_keypress()
    
    def run(self):
        while self.running:
        
            self.clock.tick(self.update_rate)
            
            self.handle_events()
            
            if self.resetter:
                self.resetter.check_user_bounds()
            
            for i, user in enumerate(self.users):
                if self.resetter and user.resetting:
                    self.resetter.reset(user, self.clock.get_time())
                    user.reset_movement()
                    self.redirector.set_user_virtual_theta(i, user.rot_virtual)
                    self.redirector.set_user_real_theta(i, user.rot_real)
                                    
                else:
                    user.move(self.clock.get_time())
                
                    pos_r = []
                    rot_r = []
                    pos_r.append(user.pos_real)
                    rot_r.append(user.rot_real)

                    old_rot = rot_vector(user.rot_virtual)
                    pos_v, rot_v = self.redirector.redirect(pos_r, rot_r)
                
                    user.pos_virtual = pos_v[0]
                    user.rot_virtual = rot_v[0]
                    user.rot_real = self.redirector.get_user_real_theta(i)

            
            if self.gui:
                self.gui.draw_background()
                self.redirector.draw_real(self.gui.view_real, self.gui.ppm_r)
                self.redirector.draw_virtual(self.gui.view_virtual, self.gui.ppm_v)
                for user in self.users:
                    user.draw_real(self.gui.view_real, self.gui.ppm_r)
                    user.draw_virtual(self.gui.view_virtual, self.gui.ppm_v)
                self.gui.draw_finished()
            
            
