"""
1) Obtain gains
2) Handle events
3) 
"""

#
#
#

import pygame
from math import pi, cos, sin

from util import *


class User(object):
    default_controls_1 = {'forward': pygame.K_w, 'back': pygame.K_s, 'cw': pygame.K_d, 'ccw': pygame.K_a}
    default_controls_2 = {'forward': pygame.K_UP, 'back': pygame.K_DOWN, 'cw': pygame.K_RIGHT, 'ccw': pygame.K_LEFT}
    
    def __init__(self, pos_real=[0.0,0.0], rot_real=pi/2.0, pos_virtual=[0.0,0.0], rot_virtual=pi/2.0):
        self.pos_real = pos_real        # [meters, meters]
        self.rot_real = rot_real        # radians
        self.pos_virtual = pos_virtual  # [meters, meters]
        self.rot_virtual = rot_virtual  # radians
        self.color = (0,200,0)          # (red,green,blue)
        self.speed_translation = 2.0    # meters/second
        self.speed_rotation = pi        # radians/second
        
        self.controls = User.default_controls_1
        self.movement = {'forward': False, 'back': False, 'cw': False, 'ccw': False}
        
    def draw_real(self, context, ppm):
        """ Draws the real representation of the user """
        position = (int(self.pos_real[0] * ppm), int(self.pos_real[1] * ppm))
        rotation = rot_vector(self.rot_real)
        pygame.draw.circle(context, self.color, position, 10)
        pygame.draw.line(context, (0,0,0), position, (position[0] + int(rotation[0] * ppm * 0.5), position[1] + int(rotation[1] * ppm * 0.5)))
        
    def draw_virtual(self, context, ppm):
        """ Draws the virtual representation of the user """
        position = (int(self.pos_virtual[0] * ppm), int(self.pos_virtual[1] * ppm))
        rotation = rot_vector(self.rot_virtual)
        pygame.draw.circle(context, self.color, position, 10)
        pygame.draw.line(context, (0,0,0), position, (position[0] + int(rotation[0] * ppm * 0.5), position[1] + int(rotation[1] * ppm * 0.5)))

    def handle_keypress(self):
        """ Handles user keypresses """
        keys = pygame.key.get_pressed()
        for val in self.controls:
            if keys[self.controls[val]]:
                self.movement[val] = True
                
    
    def move(self, delta_t):
        """ Adjusts position and rotation """
        translation_virtual = 0.0
        rotation_virtual = 0.0
        translation_real = 0.0
        rotation_real = 0.0
        
        # Determine real movement
        if self.movement['forward']:
            translation_real = self.speed_translation * (delta_t / 1000.0)
        #elif self.movement['back']:
        #    translation_real = -1 * self.speed_translation * (delta_t / 1000.0)
        elif self.movement['cw']:
            rotation_real = self.speed_rotation * (delta_t / 1000.0)
        elif self.movement['ccw']:
            rotation_real = -1 * self.speed_rotation * (delta_t / 1000.0)
        
        # Determine virtual movement
        #translation_virtual = translation_real / gain_t
        #rotation_virtual = rotation_real / gain_r
        
        # Update positions
        #self.pos_virtual[0] += translation_virtual * self.rot_virtual[0]
        #self.pos_virtual[1] += translation_virtual * self.rot_virtual[1]
        self.pos_real[0] += translation_real * cos(self.rot_real)
        self.pos_real[1] += translation_real * sin(self.rot_real)
        
        # Update rotations
        #x = (self.rot_virtual[0] * cos(rotation_virtual)) - (self.rot_virtual[1] * sin(rotation_virtual))
        #y = (self.rot_virtual[1] * cos(rotation_virtual)) + (self.rot_virtual[0] * sin(rotation_virtual))
        #self.rot_virtual = normalize([x, y])
        #x = (self.rot_real[0] * cos(rotation_real)) - (self.rot_real[1] * sin(rotation_real))
        #y = (self.rot_real[0] * sin(rotation_real)) + (self.rot_real[1] * cos(rotation_real))
        #self.rot_real = normalize([x, y])
        self.rot_real += rotation_real
        
        self.reset_movement()
        
        while self.rot_real < 0.0:
            self.rot_real += 2 * pi
        while self.rot_real >= 2 * pi:
            self.rot_real -= 2 * pi
        while self.rot_virtual < 0.0:
            self.rot_virtual += 2 * pi
        while self.rot_virtual >= 2 * pi:
            self.rot_virtual -= 2 * pi 
            
    def reset_movement(self):
        # Reset movement bools
        for val in self.movement:
            self.movement[val] = False
    
    def configuration(self):
        """ Returns the configuration space reprentation """
        return self.pos_real + self.pos_virtual
        
    def configuration_rot(self):
        return self.rot_real + rot_vector(self.rot_virtual)

        
        
