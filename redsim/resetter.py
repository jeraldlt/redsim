"""
"""

#
#
#

from math import pi, atan2, asin
from util import *

class Resetter(object):
    def __init__(self, users, rx_range, ry_range, buf):
        self.users = users
        self.rx_range = rx_range
        self.ry_range = ry_range
        self.buf = buf
        
        for user in users:
            user.resetting = False
            user.reset_complete = False
            user.last_reset = None
            
    def check_user_bounds(self):
        for user in self.users:
            
            resetting = False
            if user.pos_real[0] <= self.rx_range[0] + self.buf:
                resetting = True
            elif user.pos_real[1] <= self.ry_range[0] + self.buf:
                resetting = True
            elif user.pos_real[0] >= self.rx_range[1] - self.buf:
                resetting = True
            elif user.pos_real[1] >= self.ry_range[1] - self.buf:
                resetting = True
                
            if resetting:
                
                if not user.resetting and not user.reset_complete:
                    user.resetting = True
                    user.last_reset = user.pos_real
                    self.reset_init(user)
                    print "Resetting..."
                    
                """
                elif not user.resetting and user.reset_complete and user.last_reset and distance(user.last_reset, user.pos_real) < 0.1:
                    user.resetting = True
                    user.last_reset = user.pos_real
                    self.reset_init(user)
                    print "Resetting..."
                """    
            elif user.reset_complete:
                
                user.reset_complete = False
        
    
class TwoToOneResetter(Resetter):
    def __init__(self, users, rx_range, ry_range, buf):
        Resetter.__init__(self, users, rx_range, ry_range, buf)
        
        for user in users:
            user.reset_amount = 0.0
            
    def reset_init(self, user):
        pass
    
    def reset(self, user, delta_t):
        delta_theta = 0.0
        if user.movement["cw"]:
            delta_theta = user.speed_rotation * (delta_t / 1000.0)
        elif user.movement["ccw"]:
            delta_theta = -1 * user.speed_rotation * (delta_t / 1000.0)
        else:
            return
            
        user.rot_real += delta_theta
        user.rot_virtual += delta_theta * 2.0
        user.reset_amount += delta_theta
        
        while user.rot_real < 0.0:
            user.rot_real += 2 * pi
        while user.rot_real >= 2 * pi:
            user.rot_real -= 2 * pi
        while user.rot_virtual < 0.0:
            user.rot_virtual += 2 * pi
        while user.rot_virtual >= 2 * pi:
            user.rot_virtual -= 2 * pi 
        
        
        if abs(user.reset_amount) >= pi:
            print "Reset Complete..."
            user.resetting = False
            user.reset_complete = True
            user.reset_amount = 0.0
            
class ToTargetResetter(Resetter):
    def __init__(self, users, gr, Gr, target, rx_range, ry_range, buf):
        Resetter.__init__(self, users, rx_range, ry_range, buf)
        
        self.gr = gr
        self.Gr = Gr
        self.target = target
        self.reset_amount_target = 0.0
        
        for user in users:
            user.reset_amount = 0.0
            
    def reset_init(self, user):
        x = self.target[0] - user.pos_real[0]
        y = self.target[1] - user.pos_real[1]
        
        theta_goal = atan2(y, x)
        
        while theta_goal < 0.0:
            theta_goal += 2 * pi
        while theta_goal >= 2 * pi:
            theta_goal -= 2 * pi

        target1 = theta_goal - user.rot_real
        spin = target1 / abs(target1)
        target1 = spin * clamp(abs(target1), self.gr, self.Gr)
        print target1
        
        theta_goal -= 2 * pi
        
        target2 = theta_goal - user.rot_real
        spin = target2 / abs(target2)
        target2 = spin * clamp(abs(target2), self.gr, self.Gr)
        print target2
        
        if abs(abs(target1) - 1) < abs(abs(target2) - 1):
            self.reset_amount_target = target1
        else:
            self.reset_amount_target = target2
        print self.reset_amount_target

      
    def reset_cleanup(self, user):
        print user.reset_amount
        print "Reset Complete..."
        user.resetting = False
        user.reset_complete = True
        user.reset_amount = 0.0
    
    def reset(self, user, delta_t):
        delta_theta = 0.0
        if user.movement["cw"] and self.reset_amount_target > 0:
            delta_theta = user.speed_rotation * (delta_t / 1000.0)
        elif user.movement["ccw"] and self.reset_amount_target < 0:
            delta_theta = -1 * user.speed_rotation * (delta_t / 1000.0)
        else:
            return
            
        user.rot_real += delta_theta
        user.rot_virtual += delta_theta * 2 * pi / self.reset_amount_target
        user.reset_amount += delta_theta
        
        
        if self.reset_amount_target <= 0 and user.reset_amount <= self.reset_amount_target:
            self.reset_cleanup(user)
        elif self.reset_amount_target >=0 and user.reset_amount >= self.reset_amount_target:
            self.reset_cleanup(user)


class ToCenterResetter(ToTargetResetter):
    def __init__(self, users, gr, Gr, rx_range, ry_range, buf):
        
        self.center = ((rx_range[1] - rx_range[0]) / 2.0, (ry_range[1] - ry_range[0]) / 2.0)
        ToTargetResetter.__init__(self, users, gr, Gr, self.center, rx_range, ry_range, buf)
        
