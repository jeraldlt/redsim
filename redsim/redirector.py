"""

"""

#
#
#

import sys
sys.path.append("/home/jerald/Projects/librdw/interfaces/python")
import librdw

import pygame
from util import *


class NoneRedirector(librdw.Redirector):
    def __init__(self, users):
        librdw.Redirector.__init__(self, librdw.RDW_ALG_NONE, len(users))
        for i, user in enumerate(users):
            self.set_user_real_pos(i, user.pos_real)
            self.set_user_real_theta(i, user.rot_real)
            self.set_user_virtual_pos(i, user.pos_virtual)
            self.set_user_virtual_theta(i, user.rot_virtual)
        
    def draw_real(self, context, ppm):
        pass
    
    def draw_virtual(self, context, ppm):
        pass
        
        
class ConstantRedirector(librdw.Redirector):
    def __init__(self, users, gt, gr, k_max):
        librdw.Redirector.__init__(self, librdw.RDW_ALG_CONSTANT, len(users))
        
        self.users = users
        
        self.set_simulation(True)
        
        for i, user in enumerate(users):
            self.set_user_real_pos(i, user.pos_real)
            self.set_user_real_theta(i, user.rot_real)
            self.set_user_virtual_pos(i, user.pos_virtual)
            self.set_user_virtual_theta(i, user.rot_virtual)
        
        self.set_translation_gains(gt, gt)
        self.set_rotation_gains(gr, gr)
        self.set_max_curvature(k_max)
        
    def draw_real(self, context, ppm):
        pass
    
    def draw_virtual(self, context, ppm):
        pass


class SingleUserNavRedirector(librdw.Redirector):
    def __init__(self, users, rx_range, ry_range, vx_range, vy_range, step, epsilon, mu, gt, Gt, gr, Gr, k_max):
        librdw.Redirector.__init__(self, librdw.RDW_ALG_NAVFUNC, len(users))
        self.set_simulation(True)
        
        self.users = users
        for i, user in enumerate(users):
            self.set_user_real_pos(i, user.pos_real)
            self.set_user_real_theta(i, user.rot_real)
            self.set_user_virtual_pos(i, user.pos_virtual)
            self.set_user_virtual_theta(i, user.rot_virtual)
            
        self.rx_range = rx_range
        self.ry_range = ry_range
        self.vx_range = vx_range
        self.vy_range = vy_range
        self.step = step
        self.epsilon = epsilon
        self.mu = mu

        self.set_real_range(rx_range[0], ry_range[0], rx_range[1], ry_range[1])
        self.set_virtual_range(vx_range[0], vy_range[0], vx_range[1], vy_range[1])
        self.set_step(step)
        self.set_epsilon(epsilon)
        self.set_mu(mu)
        
        self.set_translation_gains(gt, Gt)
        self.set_rotation_gains(gr, Gr)
        self.set_max_curvature(k_max)
        
        self.targets = [((5.0, 5.0), (15.0, 15.0)), ((5.0, 5.0), (5.0, 15.0)), ((5.0, 5.0), (5.0, 5.0)), ((5.0, 5.0), (15.0, 5.0))]
        self.cur_target = 0
        
        self.xl = [self.targets[0][0][0], self.targets[0][0][1], self.targets[0][1][0], self.targets[0][1][1]]
        
        self.set_user_real_pos_goal(0, self.targets[0][0])
        self.set_user_real_theta_goal(0, self.users[0].rot_real)
        self.set_user_virtual_pos_goal(0, self.targets[0][1])
        self.set_user_virtual_theta_goal(0, self.users[0].rot_virtual)
        
    
    def redirect(self, user_pos, user_theta):
        self.update_target()
    
        self.set_user_real_pos_goal(0, self.targets[0][0])
        self.set_user_real_theta_goal(0, self.users[0].rot_real)
        self.set_user_virtual_pos_goal(0, self.targets[0][1])
        self.set_user_virtual_theta_goal(0, self.users[0].rot_virtual)
        
        return librdw.Redirector.redirect(self, user_pos, user_theta)
        
    def update_target(self):
        if (distance(self.users[0].pos_virtual, self.targets[self.cur_target][1]) <= 0.1):
            self.cur_target += 1
            if self.cur_target == len(self.targets):
                self.cur_target = 0
                
            target = self.targets[self.cur_target]
            self.xl[0] = target[0][0]
            self.xl[1] = target[0][1]
            self.xl[2] = target[1][0]
            self.xl[3] = target[1][1]
        
        
    def draw_real(self, context, ppm):
        pygame.draw.circle(context, (255,0,0), (int(self.xl[0] * ppm), int(self.xl[1] * ppm)), 5)
    
    def draw_virtual(self, context, ppm):
        pygame.draw.circle(context, (255,0,0), (int(self.xl[2] * ppm), int(self.xl[3] * ppm)), 5)
        
        
    
