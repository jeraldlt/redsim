from redsim import *
from math import pi
rx_range = [0.0, 10.0]
ry_range = [0.0, 10.0]
vx_range = [0.0, 20.0]
vy_range = [0.0, 20.0]


u = User([5.0,5.0], pi/2.0, [10.0,10.0], pi/2.0)
r = NoneRedirector([u])
res = ToCenterResetter([u], 0.5, 2.0, rx_range, ry_range, 1.0)
#res = TwoToOneResetter([u], rx_range, ry_range, 1.0)
g = GUI(rx_range, ry_range, vx_range, vy_range)
s = Simulation([u], r, res, g)

s.run()
