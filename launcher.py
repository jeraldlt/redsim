import sys
from redsim import *
from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtWidgets import QLabel, QPushButton, QComboBox, QLineEdit, QCheckBox
from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout, QGridLayout

 
class App(QWidget):
 
    def __init__(self):
        QWidget.__init__(self)
        self.title = 'RedSim Launcher'
        self.left = 10
        self.top = 10
        self.width = 300
        self.height = 400
        self.initUI()
        
    def clear_layout(self, layout, preserveWidget=False):
        while layout.count():
            child = layout.takeAt(0)
            if child.widget():
                if preserveWidget:
                    child.widget().setParent(None)
                else:
                    child.widget().deleteLater()
 
    def run(self):
        rx_range = [0.0, float(self.rx_lineedit.text())]
        ry_range = [0.0, float(self.ry_lineedit.text())]
        vx_range = [0.0, float(self.vx_lineedit.text())]
        vy_range = [0.0, float(self.vy_lineedit.text())]
        
        user = User([rx_range[1]/2.0, ry_range[1]/2.0], pi/2.0, [vx_range[1]/2.0, vy_range[1]/2.0], pi/2.0)
        redirector = None
        gui = GUI(rx_range, ry_range, vx_range, vy_range)
        simulation = None
        
        if self.redirector_combobox.currentText() == "No Redirection":
            redirector = NoneRedirector([user])
            
        elif self.redirector_combobox.currentText() == "Constant Redirector":
            Gt = float(self.Gt_lineedit.text())
            Gr = float(self.Gr_lineedit.text())
            k = float(self.kmax_lineedit.text())
            redirector = ConstantRedirector([user], Gt, Gr, k)
        
        simulation = Simulation([user], redirector, gui)
        
        simulation.run()
        
            
    def redirector_combobox_on_activated(self, text):
        self.clear_layout(self.redirector_extras_layout, True)
        
        if text == "Steer to Orbit":
            self.redirector_extras_layout.addWidget(QLabel("Orbit Radius"), 0, 1)
            self.redirector_extras_layout.addWidget(self.orbit_radius_lineedit, 0, 2)
            
        elif text == "NavFunc":
            self.redirector_extras_layout.addWidget(QLabel("Epsilon"), 0, 1)
            self.redirector_extras_layout.addWidget(self.epsilon_lineedit, 0, 2)
            self.redirector_extras_layout.addWidget(QLabel("Mu"), 1, 1)
            self.redirector_extras_layout.addWidget(self.mu_lineedit, 1, 2)
            self.redirector_extras_layout.addWidget(QLabel("Step"), 2, 1)
            self.redirector_extras_layout.addWidget(self.step_lineedit, 2, 2)
    
    def resetter_combobox_on_activated(self, text):
        self.clear_layout(self.resetter_extras_layout, True)
        
        if text == "Center Resetter":
            self.resetter_extras_layout.addWidget(self.center_resetter_constrain_rotation, 0, 1)
        
    def controller_combobox_on_activated(self, text):
        self.clear_layout(self.controller_extras_layout, True)
        
        if text == "Keyboard":
            self.controller_extras_layout.addWidget(QLabel("Controls"), 0, 1)
            self.controller_extras_layout.addWidget(self.keyboard_controller_combobox, 0, 2)
            
        elif text == "Waypoint Walker":
            self.controller_extras_layout.addWidget(QLabel("Waypoints File"), 0, 1)
            self.controller_extras_layout.addWidget(self.waypoints_file_lineedit, 0, 2)
               
    
    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
 
        self.redirector_combobox = QComboBox(self)
        self.redirector_combobox.activated[str].connect(self.redirector_combobox_on_activated)
        self.redirector_combobox.addItem("No Redirection")
        self.redirector_combobox.addItem("Constant Redirector")
        self.redirector_combobox.addItem("Steer to Center")
        self.redirector_combobox.addItem("Steer to Orbit")
        self.redirector_combobox.addItem("NavFunc")
        
        self.resetter_combobox = QComboBox(self)
        self.resetter_combobox.activated[str].connect(self.resetter_combobox_on_activated)
        self.resetter_combobox.addItem("No Resets")
        self.resetter_combobox.addItem("2:1 Resetter")
        self.resetter_combobox.addItem("Center Resetter")
        
        self.controller_combobox = QComboBox(self)
        self.controller_combobox.activated[str].connect(self.controller_combobox_on_activated)
        self.controller_combobox.addItem("Keyboard")
        self.controller_combobox.addItem("Random Walker")
        self.controller_combobox.addItem("Waypoint Walker")
        
        self.rx_lineedit = QLineEdit()
        self.rx_lineedit.setText("10.0")
        self.gr_lineedit = QLineEdit()
        self.ry_lineedit = QLineEdit()
        self.ry_lineedit.setText("10.0")
        
        self.vx_lineedit = QLineEdit()
        self.vx_lineedit.setText("20.0")
        self.vy_lineedit = QLineEdit()
        self.vy_lineedit.setText("20.0")
        
        self.gt_lineedit = QLineEdit()
        self.gt_lineedit.setText("0.75")
        self.Gt_lineedit = QLineEdit()
        self.Gt_lineedit.setText("1.5")
        
        self.gr_lineedit = QLineEdit()
        self.gr_lineedit.setText("0.75")
        self.gr_lineedit.setToolTip("Minimum rotation gain")
        self.Gr_lineedit = QLineEdit()
        self.Gr_lineedit.setText("1.5")
        
        self.kmin_lineedit = QLineEdit()
        self.kmin_lineedit.setText("0.0")
        self.kmax_lineedit = QLineEdit()
        self.kmax_lineedit.setText("0.13")
        
        self.run_button = QPushButton("Run")
        self.run_button.clicked.connect(self.run)
        
        
        self.orbit_radius_lineedit = QLineEdit()
        self.orbit_radius_lineedit.setText("6")
        
        self.epsilon_lineedit = QLineEdit()
        self.epsilon_lineedit.setText("0.01")
        self.mu_lineedit = QLineEdit()
        self.mu_lineedit.setText("3.0")
        self.step_lineedit = QLineEdit()
        self.step_lineedit.setText("0.05")
        
        self.center_resetter_constrain_rotation = QCheckBox("Constrain rotation")
        
        self.keyboard_controller_combobox = QComboBox()
        self.keyboard_controller_combobox.addItem("WASD")
        self.keyboard_controller_combobox.addItem("Arrows")
        
        self.waypoints_file_lineedit = QLineEdit()
        
        
        master_layout = QVBoxLayout()
        
        redirector_layout = QHBoxLayout()
        redirector_layout.addWidget(QLabel("Redirector"))
        redirector_layout.addWidget(self.redirector_combobox)
        self.redirector_extras_layout = QGridLayout()
        self.redirector_extras_layout.setColumnMinimumWidth(0, 50)
        master_layout.addLayout(redirector_layout)
        master_layout.addLayout(self.redirector_extras_layout)

        resetter_layout = QHBoxLayout()
        resetter_layout.addWidget(QLabel("Resetter"))
        resetter_layout.addWidget(self.resetter_combobox)
        self.resetter_extras_layout = QGridLayout()
        self.resetter_extras_layout.setColumnMinimumWidth(0, 50)
        master_layout.addLayout(resetter_layout)
        master_layout.addLayout(self.resetter_extras_layout)
        
        controller_layout = QHBoxLayout()
        controller_layout.addWidget(QLabel("Controller"))
        controller_layout.addWidget(self.controller_combobox)
        self.controller_extras_layout = QGridLayout()
        self.controller_extras_layout.setColumnMinimumWidth(0, 50)
        master_layout.addLayout(controller_layout)
        master_layout.addLayout(self.controller_extras_layout)
        
        real_environment_layout = QHBoxLayout()
        real_environment_layout.addWidget(QLabel("rx_ub"))
        real_environment_layout.addWidget(self.rx_lineedit)
        real_environment_layout.addWidget(self.ry_lineedit)
        real_environment_layout.addWidget(QLabel("ry_ub"))
        master_layout.addLayout(real_environment_layout)
        
        virtual_environment_layout = QHBoxLayout()
        virtual_environment_layout.addWidget(QLabel("vx_ub"))
        virtual_environment_layout.addWidget(self.vx_lineedit)
        virtual_environment_layout.addWidget(self.vy_lineedit)
        virtual_environment_layout.addWidget(QLabel("vy_ub"))
        master_layout.addLayout(virtual_environment_layout)
        
        translation_gain_layout = QHBoxLayout()
        translation_gain_layout.addWidget(QLabel("gt"))
        translation_gain_layout.addWidget(self.gt_lineedit)
        translation_gain_layout.addWidget(self.Gt_lineedit)
        translation_gain_layout.addWidget(QLabel("Gt"))
        master_layout.addLayout(translation_gain_layout)
        
        rotation_gain_layout = QHBoxLayout()
        rotation_gain_layout.addWidget(QLabel("gr"))
        rotation_gain_layout.addWidget(self.gr_lineedit)
        rotation_gain_layout.addWidget(self.Gr_lineedit)
        rotation_gain_layout.addWidget(QLabel("Gr"))
        master_layout.addLayout(rotation_gain_layout)
        
        kmax_gain_layout = QHBoxLayout()
        kmax_gain_layout.addWidget(QLabel("k_min"))
        kmax_gain_layout.addWidget(self.kmin_lineedit)
        kmax_gain_layout.addWidget(self.kmax_lineedit)
        kmax_gain_layout.addWidget(QLabel("k_max"))
        master_layout.addLayout(kmax_gain_layout)
        
        master_layout.addWidget(self.run_button)

        self.setLayout(master_layout) 
        self.redirector_combobox_on_activated(self.redirector_combobox.currentText())
        self.resetter_combobox_on_activated(self.resetter_combobox.currentText())
        self.controller_combobox_on_activated(self.controller_combobox.currentText())
        self.show()
        
 
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
